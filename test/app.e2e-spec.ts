import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication, VersioningType } from '@nestjs/common';
import { initializeApp, defaultVersion } from '../src/main';
import { AppModule } from '../src/app.module';

describe('With global default version', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    app.useLogger(undefined);
    initializeApp(app, true);

    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });

  it('GET /v1 should reply with "v1"', () =>
    request(app.getHttpServer())
      .get('/v1')
      .expect(200)
      .expect('v1')
  );

  it('GET /v2 should reply with "v2"', () =>
    request(app.getHttpServer())
      .get('/v2')
      .expect(200)
      .expect('v2')
  );

  describe('AppControllerBadVersion', () => {
    it('GET /v123456789 should reply with Not Found', () =>
      request(app.getHttpServer())
        .get('/v123456789')
        .expect(404)
    );
  });

  describe('ControllerWithNoVersions', () => {
    // XXX: The new behavior
    it(`GET / should reply with Not Found since there's some global default version`, () =>
      request(app.getHttpServer())
        .get('/')
        .expect(404)
    );

    it(`GET /v2 should reply with "v2" since this route was override by the AppController#getHelloV2`, () =>
      request(app.getHttpServer())
        .get('/v2')
        .expect(200)
        .expect('v2')
    );

    it('GET /v2/another reply with "another one"', () =>
      request(app.getHttpServer())
        .get('/v2/another')
        .expect(200)
        .expect('another one')
    );
  });
});

describe('Without global default version', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useLogger(undefined);
    initializeApp(app, false);

    await app.init();
  });

  afterEach(async () => {
    await app.close();
  });


  describe('ControllerWithNoVersions', () => {
    // XXX: The old behavior doens't change for this app
    it(`GET / should reply with "with no versioning"`, () =>
      request(app.getHttpServer())
        .get('/')
        .expect(200)
        .expect('with no versioning metadata')
    );

    it('GET /another reply with "another one"', () =>
      request(app.getHttpServer())
        .get('/another')
        .expect(200)
        .expect('another one')
    );
  });
});
