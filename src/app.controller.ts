import { Controller, Get, Version } from '@nestjs/common';

@Controller({ version: '1' })
export class AppController {
  @Get()
  getHelloV1(): string {
    return 'v1';
  }

  @Version('2')
  @Get()
  getHelloV2(): string {
    return 'v2';
  }
}

@Controller({ version: '123456789' })
export class AppControllerBadVersion {
  @Get('bad')
  getHello(): string {
    return 'bad version';
  }
}

@Controller()
export class ControllerWithNoVersion {
  @Get()
  getHello(): string {
    return 'with no versioning metadata';
  }

  @Get('another')
  getAnother(): string {
    return 'another one';
  }
}
