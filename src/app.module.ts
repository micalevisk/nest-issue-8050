import { Module } from '@nestjs/common';
import { 
  AppController,
  AppControllerBadVersion,
  ControllerWithNoVersion,
} from './app.controller';

@Module({
  controllers: [
    AppController,
    AppControllerBadVersion,
    ControllerWithNoVersion,
  ],
})
export class AppModule {}
