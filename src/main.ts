import { NestFactory } from '@nestjs/core';
import { VersioningType, INestApplication } from '@nestjs/common';
import { AppModule } from './app.module';

export const defaultVersion = '2';

export function initializeApp(app: INestApplication, withDefaultVersion: boolean) {
  app.enableVersioning({
    type: VersioningType.URI,
    prefix: 'v',
    // @ts-ignore
    defaultVersion: withDefaultVersion ? defaultVersion : undefined,
  });
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  initializeApp(app, true);

  await app.listen(3000);
}

if (require.main === module) {
  bootstrap();
}
